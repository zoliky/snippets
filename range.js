// Generate an array of numbers, with values ranging from
// start (inclusive) to stop (exclusive)

const range = (start, stop) =>
  new Array(stop - start).fill(0).map((v, i) => start + i);

let from2To6 = range(2, 7); // [2, 3, 4, 5, 6] 
