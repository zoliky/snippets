// Reverse a string

const str = "Leslie";
const result = str.split("").reduceRight((x, y) => x + y, "");
